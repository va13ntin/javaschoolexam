package com.tsystems.javaschool.tasks.pyramid;

import java.util.Collections;
import java.util.List;

public class PyramidBuilder {

    /**
     *
     * @param listSize - size of the inputNumbers list
     * @return pyramid height if the input listSize is correct. In the other case returns -1
     */
    private int pyramidHeight(int listSize) {
        int sum = 0;

        for (int i = 1; ; ++i) {
            sum += i;

            if (listSize == sum) {
                return i;
            }
            else if (listSize < sum) {
                return -1;
            }
        }
    }

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        //make sure if list consists only numbers
        try {
            Collections.sort(inputNumbers);
        }
        catch (Throwable exc) {
            throw new CannotBuildPyramidException();
        }

        int pyrH = pyramidHeight(inputNumbers.size());

        //throw an exception if list haven't enough numbers to create a pyramid
        if (pyrH == -1) throw new CannotBuildPyramidException();

        int[][] resArr = new int[pyrH][2*pyrH - 1];

        int elemIndex = 0;
        for (int h = 0; h < pyrH; ++h) {
            int indent = (pyrH - 1) - h;
            int bias = 0;

            for (int elementsNumber = h + 1; elementsNumber > 0; --elementsNumber) {
                resArr[h][indent + bias] = inputNumbers.get(elemIndex);

                bias += 2;
                elemIndex += 1;
            }
        }

        return resArr;
    }


}
