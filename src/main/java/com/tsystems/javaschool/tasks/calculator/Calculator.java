package com.tsystems.javaschool.tasks.calculator;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class Calculator {

    enum Operation {
        LEFT_BRACKET("(", 0), RIGHT_BRACKET(")", 0), PLUS("+", 1), MINUS("-", 1), MULTIPLY("*", 2), DIVIDE("/", 2);

        String operation;
        int priority;

        Operation(String operation, int priority) {
            this.operation = operation;
            this.priority = priority;
        }

        static boolean isOperation(String checkedStr) {
            for (Operation oper: Operation.values()) {
                if (oper.operation.equals(checkedStr)) return true;
            }
            return false;
        }
    }

    private Operation getOperation(String operation) {
        for (Operation oper: Operation.values()) {
            if (oper.operation.equals(operation)) {
                return oper;
            }
        }
        return null;
    }

    private String result(List<String> postfix) {
        int i = 0;

        while (postfix.size() > 1) {
            if (Operation.isOperation(postfix.get(i))) {
                switch (postfix.get(i)) {
                    case "+":
                        postfix.set(i - 2,
                                String.valueOf(Double.valueOf(postfix.get(i - 2)) + Double.valueOf(postfix.get(i - 1))));
                        postfix.remove(i - 1);
                        postfix.remove(i - 1);
                        i -= 2;
                        break;
                    case "-":
                        postfix.set(i - 2,
                                String.valueOf(Double.valueOf(postfix.get(i - 2)) - Double.valueOf(postfix.get(i - 1))));
                        postfix.remove(i - 1);
                        postfix.remove(i - 1);
                        i -= 2;
                        break;
                    case "/":
                        postfix.set(i - 2,
                                String.valueOf(Double.valueOf(postfix.get(i - 2)) / Double.valueOf(postfix.get(i - 1))));
                        postfix.remove(i - 1);
                        postfix.remove(i - 1);
                        i -= 2;
                        break;
                    case "*":
                        postfix.set(i - 2,
                                String.valueOf(Double.valueOf(postfix.get(i - 2)) * Double.valueOf(postfix.get(i - 1))));
                        postfix.remove(i - 1);
                        postfix.remove(i - 1);
                        i -= 2;
                        break;
                }
            }
            else {
                ++i;
            }
        }

        return new BigDecimal(postfix.get(0)).setScale(4, BigDecimal.ROUND_HALF_DOWN).toString();
    }

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        try {
            char[] charStatement = statement.toCharArray();

            //make sure if a statement has the same number of left and right brackets
            int parenthesesCounter = 0;
            for (int i = 0; i < charStatement.length; ++i) {
                char ch = charStatement[i];

                if (ch == '(') {
                    ++parenthesesCounter;
                } else if (ch == ')') {
                    --parenthesesCounter;
                }

                if (parenthesesCounter < 0) return null;

                //and also check there is no one ',' instead of '.'
                if (ch == ',') {
                    return null;
                }

                //aaaand we also have to know if there any double operations like '**' or '++' which are mistakes
                if (ch == '+' || ch == '-' || ch == '/' || ch == '*' || ch == '.') {

                    if (i != charStatement.length - 1) {
                        if (charStatement[i + 1] == ch) {
                            return null;
                        }
                    }
                }
            }

            if (parenthesesCounter != 0) return null;

            //get an expression to postfix form (use reverse polish entry)
            List<String> postfix = new ArrayList<>();
            Stack<Operation> stack = new Stack<>();

            for (int i = 0; i < charStatement.length; ++i) {
                if (Operation.isOperation(String.valueOf(charStatement[i]))) { //is operation
                    Operation operation = getOperation(String.valueOf(charStatement[i]));

                    if (operation.equals(Operation.RIGHT_BRACKET)) {
                        while (!stack.peek().equals(Operation.LEFT_BRACKET)) {
                            postfix.add(stack.pop().operation);
                        }
                        stack.pop();
                        continue;
                    }

                    if (operation.equals(Operation.LEFT_BRACKET)) {
                        stack.push(operation);
                        continue;
                    }

                    while (!stack.isEmpty() && stack.peek().priority >= operation.priority) {
                        postfix.add(stack.pop().operation);
                    }
                    stack.push(operation);
                }
                else {                                                      //is operand(number)
                    String numb = String.valueOf(charStatement[i]);

                    while (i < (charStatement.length - 1) &&
                            !Operation.isOperation(String.valueOf(charStatement[i + 1]))) {
                        numb += charStatement[i + 1];
                        ++i;
                    }

                    postfix.add(numb);
                }
            }

            while (!stack.isEmpty()) {
                postfix.add(stack.pop().operation);
            }

            String res = result(postfix);

            //condition to passed the tests (we have to delete insignificant zeros)
            while (res.indexOf('.') != -1) {
                char ch = res.charAt(res.length() - 1);
                if (ch == '0' || ch == '.') {
                    res = res.substring(0, res.length() - 1);
                }
                else {
                    break;
                }
            }

            return res;
        }
        catch (Throwable exc) {
            return null;
        }
    }

}
